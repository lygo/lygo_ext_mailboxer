package mailboxer_client

import (
	"bitbucket.org/lygo/lygo_commons/lygo"
	"bitbucket.org/lygo/lygo_commons/lygo_errors"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_regex"
	"bitbucket.org/lygo/lygo_ext_mailboxer/mailboxer_client/drivers"
	"bitbucket.org/lygo/lygo_ext_mailboxer/mailboxer_commons"
)

type MailBoxerClient struct {
	settings *mailboxer_commons.MailboxerClientSettings
	driver   drivers.IDriver
}

// ---------------------------------------------------------------------------------------------------------------------
//	MailBoxerClient
// ---------------------------------------------------------------------------------------------------------------------

func (instance *MailBoxerClient) String() string {
	m := map[string]interface{}{
		"settings": instance.settings,
	}
	return lygo.Convert.ToString(m)
}

func (instance *MailBoxerClient) Open() error {
	if nil == instance.driver {
		driver, err := getDriver(instance.settings)
		if nil != err {
			return err
		}
		instance.driver = driver
	}
	return instance.driver.Open()
}

func (instance *MailBoxerClient) Close() error {
	if nil != instance.driver {
		err := instance.driver.Close()
		instance.driver = nil
		return err
	}
	return nil
}

func (instance *MailBoxerClient) ListMailboxes() ([]*mailboxer_commons.MailboxInfo, error) {
	if nil != instance.driver {
		return instance.driver.ListMailboxes()
	}
	return nil, nil
}

func (instance *MailBoxerClient) GetMailboxFlags(mailboxName string) ([]string, error) {
	if nil != instance.driver {
		return instance.driver.GetMailboxFlags(mailboxName)
	}
	return nil, nil
}

func (instance *MailBoxerClient) ReadMailbox(mailboxName string, onlyNew bool) ([]*mailboxer_commons.MailboxerMessage, error) {
	if nil != instance.driver {
		return instance.driver.ReadMailbox(mailboxName, onlyNew)
	}
	return nil, nil
}

func (instance *MailBoxerClient) ReadMessage(seqNum interface{}) (*mailboxer_commons.MailboxerMessage, error) {
	if nil != instance.driver {
		return instance.driver.ReadMessage(seqNum)
	}
	return nil, nil
}
func (instance *MailBoxerClient) MarkMessageAsSeen(seqNum interface{}) error {
	if nil != instance.driver {
		return instance.driver.MarkMessageAsSeen(seqNum)
	}
	return nil
}
func (instance *MailBoxerClient) MarkMessageAsAnswered(seqNum interface{}) error {
	if nil != instance.driver {
		return instance.driver.MarkMessageAsAnswered(seqNum)
	}
	return nil
}
func (instance *MailBoxerClient) MarkMessageAsDeleted(seqNum interface{}) error {
	if nil != instance.driver {
		return instance.driver.MarkMessageAsDeleted(seqNum)
	}
	return nil
}
func (instance *MailBoxerClient) MarkMessageAsFlagged(seqNum interface{}) error {
	if nil != instance.driver {
		return instance.driver.MarkMessageAsFlagged(seqNum)
	}
	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------------------------
//	S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func NewMailBoxerClient(params ...interface{}) (*MailBoxerClient, error) {
	instance := new(MailBoxerClient)
	instance.settings = new(mailboxer_commons.MailboxerClientSettings)
	switch len(params) {
	case 1:
		// settings
		if s, b := params[0].(string); b {
			if lygo_regex.IsValidJsonObject(s) {
				err := lygo_json.Read(s, &instance.settings)
				if nil != err {
					return nil, err
				}
				return instance, nil
			} else {
				// try loading data from file
				text, err := lygo_io.ReadTextFromFile(s)
				if nil != err {
					return nil, err
				}
				return NewMailBoxerClient(text)
			}
		} else if settings, b := params[0].(*mailboxer_commons.MailboxerClientSettings); b {
			err := instance.settings.LoadFromText(lygo_json.Stringify(settings))
			if nil != err {
				return nil, err
			}
			return instance, nil
		} else if settings, b := params[0].(mailboxer_commons.MailboxerClientSettings); b {
			err := instance.settings.LoadFromText(lygo_json.Stringify(settings))
			if nil != err {
				return nil, err
			}
			return instance, nil
		} else if settings, b := params[0].(map[string]interface{}); b {
			err := instance.settings.LoadFromText(lygo_json.Stringify(settings))
			if nil != err {
				return nil, err
			}
			return instance, nil
		}
	default:
		return nil, mailboxer_commons.ErrorMismatchConfiguration
	}
	return instance, nil
}

func getDriver(settings *mailboxer_commons.MailboxerClientSettings) (drivers.IDriver, error) {
	dtype := settings.Type
	switch dtype {
	case "imap":
		return drivers.NewDriverImap(settings)
	}
	return nil, lygo_errors.Prefix(mailboxer_commons.ErrorDriverNotFound, dtype+": ")
}
