package drivers

import "bitbucket.org/lygo/lygo_ext_mailboxer/mailboxer_commons"

type IDriver interface {
	Open() error
	Close() error
	ListMailboxes() ([]*mailboxer_commons.MailboxInfo, error)
	GetMailboxFlags(mailboxName string) ([]string, error)
	ReadMailbox(mailboxName string, onlyNew bool) ([]*mailboxer_commons.MailboxerMessage, error)
	ReadMessage(uid interface{}) (*mailboxer_commons.MailboxerMessage, error)
	MarkMessageAsSeen(uid interface{}) error
	MarkMessageAsAnswered(uid interface{}) error
	MarkMessageAsDeleted(uid interface{}) error
	MarkMessageAsFlagged(uid interface{}) error
}
