package drivers

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_ext_mailboxer/mailboxer_commons"
	"github.com/emersion/go-imap"
	"github.com/emersion/go-imap/client"
	"log"
	"testing"
)

func TestRead(t *testing.T) {
	s, err := settings()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	myimap, err := NewDriverImap(s)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	err = myimap.Open()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	_, err = myimap.ReadMailbox("", false)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	first := uint32(0)
	firstMessage, err := myimap.ReadMessage(first)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	_, _ = lygo_io.WriteTextToFile(firstMessage.String(), "./first-email.json")

	// close
	err = myimap.Close()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
}

func TestReadOne(t *testing.T) {
	s, err := settings()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	myimap, err := NewDriverImap(s)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	err = myimap.Open()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// select mailbox
	_, err = myimap.ReadMailbox("", false)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	for i:=0;i<10;i++{
		firstMessage, err := myimap.ReadMessage(i)
		if nil==firstMessage || firstMessage.IsEmpty(){
			continue
		}
		if nil != err {
			t.Error(err)
			t.FailNow()
		}

		isRoot := firstMessage.IsRootMessage()
		log.Println("*", isRoot, firstMessage.Header.Subject)
		log.Println("\tParent", firstMessage.Header.ParentMessageId)
		log.Println("\tId", firstMessage.Header.MessageId)

		// _ = firstMessage.SaveToFile("./first-email.json")
		// _ = firstMessage.SaveToFile("./first-email.txt")

		log.Println("Text:", firstMessage.PlainText())
	}


	// close
	err = myimap.Close()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

}
func settings() (*mailboxer_commons.MailboxerClientSettings, error) {
	response := new(mailboxer_commons.MailboxerClientSettings)
	err := response.LoadFromFile("./settings_test.json")
	if nil != err {
		return nil, err
	}
	return response, nil
}

func readEmail(c *client.Client, box *imap.MailboxStatus) {
	// List mailboxes
	mailboxes := make(chan *imap.MailboxInfo, 10)
	done := make(chan error, 1)
	go func() {
		done <- c.List("", "*", mailboxes)
	}()

	log.Println("Mailboxes:")
	for m := range mailboxes {
		log.Println("* " + m.Name)
	}

	if err := <-done; err != nil {
		log.Fatal(err)
	}

	// Select INBOX
	//mbox, err := c.Select("INBOX", false)
	/*if err != nil {
		log.Fatal(err)
	}*/
	log.Println("Flags for INBOX:", box.Flags)

	// Get the last 4 messages
	from := uint32(1)
	to := box.Messages
	if box.Messages > 3 {
		// We're using unsigned integers here, only subtract if the result is > 0
		from = box.Messages - 3
	}
	seqset := new(imap.SeqSet)
	seqset.AddRange(from, to)

	messages := make(chan *imap.Message, 10)
	done = make(chan error, 1)
	go func() {
		done <- c.Fetch(seqset, []imap.FetchItem{imap.FetchEnvelope}, messages)
	}()
	log.Println("Last 4 messages:")
	for msg := range messages {
		log.Println("* " + msg.Envelope.Subject)
	}
	if err := <-done; err != nil {
		log.Fatal(err)
	}

	log.Println("Done!")
}
