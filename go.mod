module bitbucket.org/lygo/lygo_ext_mailboxer

go 1.16

require (
	bitbucket.org/lygo/lygo_commons v0.1.120
	bitbucket.org/lygo/lygo_ext_html v0.1.8 // indirect
	bitbucket.org/lygo/lygo_ext_nlp v0.1.11 // indirect
	github.com/cention-sany/utf7 v0.0.0-20170124080048-26cad61bd60a // indirect
	github.com/emersion/go-imap v1.1.0 // indirect
	github.com/emersion/go-sasl v0.0.0-20200509203442-7bfe0ed36a21 // indirect
	github.com/gogs/chardet v0.0.0-20191104214054-4b6791f73a28 // indirect
	github.com/google/uuid v1.3.0 // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
	golang.org/x/text v0.3.6 // indirect
)
