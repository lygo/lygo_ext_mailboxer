package _test

import (
	"bitbucket.org/lygo/lygo_ext_mailboxer/mailboxer_client"
	"bitbucket.org/lygo/lygo_ext_mailboxer/mailboxer_commons"
	_ "embed"
	"fmt"
	"github.com/emersion/go-imap"
	"github.com/emersion/go-imap/client"
	"log"
	"testing"
)

//go:embed settings.json
var defSettings string

func TestClientCreation(t *testing.T) {

	client, err := mailboxer_client.NewMailBoxerClient(defSettings)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(client)

	client, err = mailboxer_client.NewMailBoxerClient(&mailboxer_commons.MailboxerClientSettings{
		Type: "",
		Host: "",
		Port: 0,
		Auth: nil,
	})
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(client)

	client, err = mailboxer_client.NewMailBoxerClient(mailboxer_commons.MailboxerClientSettings{
		Type: "",
		Host: "",
		Port: 0,
		Auth: nil,
	})
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(client)

	client, err = mailboxer_client.NewMailBoxerClient(map[string]interface{}{
		"type": "imap", "host": "", "port": 993,
		"auth": map[string]interface{}{"user": "", "pass": ""},
	})
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(client)

}

func TestClientImap(t *testing.T) {
	client, err := mailboxer_client.NewMailBoxerClient(defSettings)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(client)

	fmt.Println("CONNECTING...")
	err = client.Open()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// do something with this connection
	fmt.Println("LIST MAILBOXES")
	mailboxes, err := client.ListMailboxes()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	for _, mailbox := range mailboxes {
		fmt.Println("*", mailbox)
	}

	fmt.Println("FLAGS for ", mailboxes[0].Name)
	flags, err := client.GetMailboxFlags(mailboxes[0].Name)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	for _, flag := range flags {
		fmt.Println("\t", "FLAG:", flag)
	}

	emails, err := client.ReadMailbox(mailboxes[0].Name, true)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	for _, email := range emails {
		fmt.Println("EMAIL", email)
	}

	fmt.Println("DISCONNECTING...")
	err = client.Close()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("DISCONNECTED!")
}

func TestClientImapRead(t *testing.T) {
	client, err := mailboxer_client.NewMailBoxerClient(defSettings)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(client)

	fmt.Println("CONNECTING...")
	err = client.Open()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	emails, err := client.ReadMailbox("", true)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	for _, email := range emails {
		fmt.Println("EMAIL", email)
	}

	fmt.Println("DISCONNECTING...")
	err = client.Close()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("DISCONNECTED!")
}

func TestSettings(t *testing.T) {
	settings := new(mailboxer_commons.MailboxerClientSettings)
	err := settings.LoadFromText(defSettings)
	if nil!=err{
		t.Error(err)
		t.FailNow()
	}
	testConn(fmt.Sprintf("%v:%v", settings.Host, settings.Port), settings.Auth.User, settings.Auth.Pass)
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func testConn(address, username, password string){
	log.Println("Connecting to server...")

	// Connect to server
	c, err := client.DialTLS(address, nil)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Connected")

	// Don't forget to logout
	defer c.Logout()

	// Login
	if err := c.Login(username, password); err != nil {
		log.Fatal(err)
	}
	log.Println("Logged in")

	// List mailboxes
	mailboxes := make(chan *imap.MailboxInfo, 10)
	done := make(chan error, 1)
	go func () {
		done <- c.List("", "*", mailboxes)
	}()

	log.Println("Mailboxes:")
	for m := range mailboxes {
		log.Println("* " + m.Name)
	}

	if err := <-done; err != nil {
		log.Fatal(err)
	}

	// Select INBOX
	mbox, err := c.Select("INBOX", false)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Flags for INBOX:", mbox.Flags)

	// Get the last 4 messages
	from := uint32(1)
	to := mbox.Messages
	if mbox.Messages > 3 {
		// We're using unsigned integers here, only subtract if the result is > 0
		from = mbox.Messages - 3
	}
	seqset := new(imap.SeqSet)
	seqset.AddRange(from, to)

	messages := make(chan *imap.Message, 10)
	done = make(chan error, 1)
	log.Println("STATUS: ", c.State())
	go func() {
		done <- c.Fetch(seqset, []imap.FetchItem{imap.FetchEnvelope}, messages)
	}()

	log.Println("Last 4 messages:")
	for msg := range messages {
		log.Println("* " + msg.Envelope.Subject)
	}

	if err := <-done; err != nil {
		log.Fatal(err)
	}

	log.Println("Done!")
}