package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_ext_mailboxer/mailboxer"
	"fmt"
	"testing"
)

func TestExampleReadEnvelope(t *testing.T) {
	// Open a sample message file.
	data, err := lygo_io.ReadTextFromFile("./mime/testdata/mail/qp-utf8-header.raw")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	message, err := mailboxer.NewMessage(data)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	fmt.Println("Date:", message.InternalDate)
	fmt.Println("Message-Id:", message.Header.MessageId)
	fmt.Println("Subject:", message.Header.Subject)
	from := message.From()
	fmt.Println("FROM:", from)
	for _, addr := range from {
		fmt.Println("\t", "*", addr.String())
	}
	fmt.Println("TO:", message.Header.To)
	fmt.Println(message.PlainText())
}
