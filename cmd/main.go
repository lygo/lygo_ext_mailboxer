package main

import (
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_strings"
	"fmt"
)

const(
	Name = "MAILBOXER"
)

func main() {
	// PANIC RECOVERY
	defer func() {
		if r := recover(); r != nil {
			// recovered from panic
			message := lygo_strings.Format("[panic] APPLICATION %s ERROR: %s. Workspace: %s",
				Name, r, lygo_paths.GetWorkspacePath())

			fmt.Println(message)
		}
	}()

}
