package mailboxer

import "bitbucket.org/lygo/lygo_ext_mailboxer/mailboxer_commons"

func NewMessage(args ...interface{}) (m *mailboxer_commons.MailboxerMessage, err error) {
	m = &mailboxer_commons.MailboxerMessage{}
	if len(args)==1{
		err = m.Parse(args[0])
	}
	return
}
