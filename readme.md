# MailBoxer #

![](./icon.png)

Email in Go made easy

![](./iso-email.png)

## How to Use ##

To use just call:

```
go get bitbucket.org/lygo/lygo_ext_mailboxer@v0.1.8
```

### Versioning ##

Sources are versioned using git tags:

```
git tag v0.1.8
git push origin v0.1.8
```

