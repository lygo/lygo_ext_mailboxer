package mailboxer_utils_test

import (
	"bitbucket.org/lygo/lygo_ext_mailboxer/mailboxer_utils"
	"testing"
)

func TestFromIDHeader(t *testing.T) {
	testCases := []struct {
		input, want string
	}{
		{"", ""},
		{"<>", ""},
		{"<%🤯>", "%🤯"},
		{"<foo@inbucket.org>", "foo@inbucket.org"},
		{"<foo%25bar>", "foo%bar"},
		{"foo+bar", "foo bar"},
	}
	for _, tc := range testCases {
		t.Run(tc.input, func(t *testing.T) {
			got := mailboxer_utils.FromIDHeader(tc.input)
			if got != tc.want {
				t.Errorf("got %q, want %q", got, tc.want)
			}
		})
	}
}

func TestToIDHeader(t *testing.T) {
	testCases := []struct {
		input, want string
	}{
		{"", "<>"},
		{"foo@inbucket.org", "<foo@inbucket.org>"},
		{"foo%bar", "<foo%25bar>"},
		{"foo bar", "<foo+bar>"},
	}
	for _, tc := range testCases {
		t.Run(tc.input, func(t *testing.T) {
			got := mailboxer_utils.ToIDHeader(tc.input)
			if got != tc.want {
				t.Errorf("got %q, want %q", got, tc.want)
			}
		})
	}
}
