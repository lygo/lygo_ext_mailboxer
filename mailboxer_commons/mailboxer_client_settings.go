package mailboxer_commons

import (
	"bitbucket.org/lygo/lygo_commons/lygo"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	_ "embed"
)

// ---------------------------------------------------------------------------------------------------------------------
//	ClientSettingsAuth
// ---------------------------------------------------------------------------------------------------------------------

type MailboxerClientSettingsAuth struct {
	User string `json:"user"`
	Pass string `json:"pass"`
}

// ---------------------------------------------------------------------------------------------------------------------
//	MailboxerClientSettings
// ---------------------------------------------------------------------------------------------------------------------

type MailboxerClientSettings struct {
	Type string                       `json:"type"`
	Host string                       `json:"host"`
	Port int                          `json:"port"`
	Tls  bool                         `json:"tls"`
	Auth *MailboxerClientSettingsAuth `json:"auth"`
}

func (instance *MailboxerClientSettings) String() string {
	return lygo_json.Stringify(instance)
}

func (instance *MailboxerClientSettings) LoadFromFile(filename string) error {
	text, err := lygo_io.ReadTextFromFile(filename)
	if nil != err {
		return err
	}
	return instance.LoadFromText(text)
}

func (instance *MailboxerClientSettings) LoadFromText(text string) error {
	return lygo_json.Read(text, &instance)
}

// ---------------------------------------------------------------------------------------------------------------------
//	S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func NewMailboxerClientSettings(stype, shost, iport, suser, spass interface{}) *MailboxerClientSettings {
	return &MailboxerClientSettings{
		Type: lygo.Convert.ToString(stype),
		Host: lygo.Convert.ToString(shost),
		Port: lygo.Convert.ToInt(iport),
		Auth: &MailboxerClientSettingsAuth{
			User: lygo.Convert.ToString(suser),
			Pass: lygo.Convert.ToString(spass),
		},
	}
}
