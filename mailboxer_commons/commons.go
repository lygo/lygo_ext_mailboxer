package mailboxer_commons

import "bitbucket.org/lygo/lygo_commons/lygo_json"

type MailboxInfo struct {
	// The mailbox attributes.
	Attributes []string `json:"attributes"`
	// The server's path separator.
	Delimiter string `json:"delimiter"`
	// The mailbox name.
	Name string `json:"name"`
}
func (instance *MailboxInfo) String() string {
	return lygo_json.Stringify(instance)
}