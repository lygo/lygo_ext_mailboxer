package mailboxer_commons

import (
	"bitbucket.org/lygo/lygo_commons/lygo"
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_date"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_strings"
	"bitbucket.org/lygo/lygo_ext_html"
	"bitbucket.org/lygo/lygo_ext_mailboxer/mailboxer_mime"
	"bytes"
	"fmt"
	"github.com/emersion/go-imap"
	"io"
	"io/ioutil"
	"net/mail"
	"strings"
	"time"
)

const (
	HeaderContentType     = "Content-Type"
	HeaderMessageId       = "Message-Id"
	HeaderParentMessageId = "Parent-Message-Id"
	HeaderDate            = "Date"
	HeaderSubject         = "Subject"
	HeaderSender          = "Sender"
	HeaderReplyTo         = "Reply-To"
	HeaderFrom            = "From"
	HeaderTo              = "To"
	HeaderCc              = "Cc"
	HeaderBcc             = "Bcc"
)

// ---------------------------------------------------------------------------------------------------------------------
//	MailboxerAttachment
// ---------------------------------------------------------------------------------------------------------------------

type MailboxerAttachment struct {
	Filename string `json:"filename"`
	Content  []byte `json:"content"`
}

// ---------------------------------------------------------------------------------------------------------------------
//	MailboxerAddress
// ---------------------------------------------------------------------------------------------------------------------

type MailboxerAddress struct {
	PersonalName string `json:"personal-name"`  // The personal name.
	AtDomainList string `json:"at-domain-list"` // The SMTP at-domain-list (source route).
	MailboxName  string `json:"mailbox-name"`   // The mailbox name.
	HostName     string `json:"host-name"`      // The host name.
}

func (instance *MailboxerAddress) Parse(text string) {
	text = strings.ReplaceAll(text, "<", ",")
	text = strings.ReplaceAll(text, ">", "")
	fields := strings.Split(text, ",")
	var name, email string
	email = text
	if len(fields) == 2 {
		name = strings.TrimSpace(fields[0])
		email = strings.TrimSpace(fields[1])
	}
	instance.PersonalName = name
	if len(email) > 0 {
		tokens := strings.Split(email, "@")
		instance.MailboxName = lygo.Arrays.GetAt(tokens, 0, "").(string)
		instance.HostName = lygo.Arrays.GetAt(tokens, 1, "").(string)
	}
}

func (instance *MailboxerAddress) String() string {
	if len(instance.PersonalName) > 0 {
		return fmt.Sprintf("%s <%s@%s>", instance.PersonalName, instance.MailboxName, instance.HostName)
	}
	return fmt.Sprintf("%s@%s", instance.MailboxName, instance.HostName)
}

func (instance *MailboxerAddress) ToMailAddress() *mail.Address {
	return &mail.Address{
		Name:    instance.PersonalName,
		Address: fmt.Sprintf("%s@%s", instance.MailboxName, instance.HostName),
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	MailboxerMessageHeader
// ---------------------------------------------------------------------------------------------------------------------

type MailboxerMessageHeader struct {
	MessageId       string              `json:"message-id"`
	ParentMessageId string              `json:"parent-message-id"`
	ReplyTo         []*MailboxerAddress `json:"reply-to"`
	Sender          []*MailboxerAddress `json:"sender"`
	From            []*MailboxerAddress `json:"from"`
	To              []*MailboxerAddress `json:"to"`
	Cc              []*MailboxerAddress `json:"cc"`
	Bcc             []*MailboxerAddress `json:"bcc"`
	Subject         string              `json:"subject"`
	Date            time.Time           `json:"date"`
}

func NewMailboxerMessageHeader() *MailboxerMessageHeader {
	instance := new(MailboxerMessageHeader)
	instance.ReplyTo = make([]*MailboxerAddress, 0)
	instance.Sender = make([]*MailboxerAddress, 0)
	instance.From = make([]*MailboxerAddress, 0)
	instance.To = make([]*MailboxerAddress, 0)
	instance.Cc = make([]*MailboxerAddress, 0)
	instance.Bcc = make([]*MailboxerAddress, 0)
	return instance
}

func (instance *MailboxerMessageHeader) String() string {
	return lygo_json.Stringify(instance)
}

func (instance *MailboxerMessageHeader) SetFrom(value string) {
	instance.SetAddresses("from", value)
}

func (instance *MailboxerMessageHeader) SetTo(value string) {
	instance.SetAddresses("to", value)
}

func (instance *MailboxerMessageHeader) SetCc(value string) {
	instance.SetAddresses("cc", value)
}

func (instance *MailboxerMessageHeader) SetBcc(value string) {
	instance.SetAddresses("bcc", value)
}

func (instance *MailboxerMessageHeader) SetSender(value string) {
	instance.SetAddresses("sender", value)
}

func (instance *MailboxerMessageHeader) SetReplyTo(value string) {
	instance.SetAddresses("reply-to", value)
}

func (instance *MailboxerMessageHeader) SetAddresses(field, value string) {
	if len(value) > 0 {
		tokens := lygo_strings.Split(value, ";,")
		for _, token := range tokens {
			address := new(MailboxerAddress)
			address.Parse(token)
			switch field {
			case "to":
				instance.To = append(instance.To, address)
			case "from":
				instance.From = append(instance.From, address)
			case "cc":
				instance.Cc = append(instance.Cc, address)
			case "bcc":
				instance.Bcc = append(instance.Bcc, address)
			case "sender":
				instance.Sender = append(instance.Sender, address)
			case "reply-to":
				instance.ReplyTo = append(instance.ReplyTo, address)
			}
		}
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	MailboxerMessageBody
// ---------------------------------------------------------------------------------------------------------------------

type MailboxerMessageBody struct {
	MIMEType          string                 `json:"mime-type"`          // The MIME type (e.g. "text", "image")
	MIMESubType       string                 `json:"mime-sub-type"`      // The MIME subtype (e.g. "plain", "png")
	Params            map[string]string      `json:"mime-params"`        // The MIME parameters.
	Id                string                 `json:"id"`                 // The Content-Id header.
	Description       string                 `json:"description"`        // The Content-Description header.
	Encoding          string                 `json:"encoding"`           // The Content-Encoding header.
	Size              uint32                 `json:"size"`               // The Content-Length header.
	Extended          bool                   `json:"extended"`           // True if the body structure contains extension data.
	Disposition       string                 `json:"disposition"`        // The Content-Disposition header field value.
	DispositionParams map[string]string      `json:"disposition-params"` // The Content-Disposition header field parameters.
	Language          []string               `json:"language"`           // The Content-Language header field, if multipart.
	Location          []string               `json:"location"`           // The content URI, if multipart.
	MD5               string                 `json:"md5"`                // The MD5 checksum.
	Multipart         bool                   `json:"multipart"`
	PartsCount        int                    `json:"parts-count"`
	Text              string                 `json:"text"`
	HTML              string                 `json:"html"`
	Attachments       []*MailboxerAttachment `json:"attachments"`
	Headers           map[string]string      `json:"headers"`
}

func NewMailboxerMessageBody() *MailboxerMessageBody {
	instance := new(MailboxerMessageBody)
	instance.Params = make(map[string]string)
	instance.DispositionParams = make(map[string]string)
	instance.Headers = make(map[string]string)
	instance.Attachments = make([]*MailboxerAttachment, 0)
	instance.Location = make([]string, 0)
	instance.Language = make([]string, 0)

	return instance
}

func (instance *MailboxerMessageBody) String() string {
	return lygo_json.Stringify(instance)
}

func (instance *MailboxerMessageBody) SetMIMEType(contentType string) {
	if len(contentType) > 0 {
		tokens := lygo_strings.Split(contentType, ";")
		if len(tokens) > 0 {
			types := lygo_strings.Split(tokens[0], "/")
			instance.MIMEType = lygo.Arrays.GetAt(types, 0, "").(string)
			instance.MIMESubType = lygo.Arrays.GetAt(types, 1, "").(string)
		}
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	MailboxerMessage
// ---------------------------------------------------------------------------------------------------------------------

type MailboxerMessage struct {
	SeqNum       uint32                  `json:"seq-num"`
	InternalDate time.Time               `json:"internal-date"` // The date the message was received by the server.
	Header       *MailboxerMessageHeader `json:"header"`
	Body         *MailboxerMessageBody   `json:"body"`
	BodyData     []byte
}

func NewMailboxerMessage() *MailboxerMessage {
	target := &MailboxerMessage{}
	target.Header = NewMailboxerMessageHeader()
	return target
}

func (instance *MailboxerMessage) Json() string {
	return lygo_json.Stringify(instance)
}

func (instance *MailboxerMessage) String() string {
	return string(instance.BodyData)
}

func (instance *MailboxerMessage) IsEmpty() bool {
	return nil == instance || nil == instance.Header || nil == instance.Body
}

func (instance *MailboxerMessage) IsRootMessage() bool {
	if nil != instance.Header {
		id := instance.Header.MessageId
		parentId := instance.Header.ParentMessageId
		if len(id) > 0 {
			return id == parentId || len(parentId) == 0
		}
	}
	return false
}

func (instance *MailboxerMessage) Subject() string {
	if nil != instance.Header && len(instance.Header.Subject) > 0 {
		return instance.Header.Subject
	}
	return "undefined"
}

func (instance *MailboxerMessage) PlainText() string {
	if nil != instance {
		if nil != instance.Body {
			if len(instance.Body.Text) > 0 {
				// first of all the text
				return instance.Body.Text
			} else if len(instance.Body.HTML) > 0 {
				// if no plain text try to parse HTML
				parser, err := lygo_ext_html.NewHtmlParser(instance.Body.HTML)
				if nil == err {
					text, renderError := parser.RenderMD()
					if nil == renderError {
						return text
					}
					return parser.TextAll()
				}
			}
		}
	}
	return ""
}

func (instance *MailboxerMessage) To() []*mail.Address {
	response := make([]*mail.Address, 0)
	if nil != instance && nil != instance.Header {
		for _, a := range instance.Header.To {
			response = append(response, a.ToMailAddress())
		}
	}
	return response
}

func (instance *MailboxerMessage) From() []*mail.Address {
	response := make([]*mail.Address, 0)
	tmp := make([]string, 0)
	if nil != instance && nil != instance.Header {
		for _, a := range instance.Header.From {
			ma := a.ToMailAddress()
			if lygo.Arrays.IndexOf(ma.String(), tmp)==-1{
				tmp = append(tmp, ma.String())
				response = append(response, ma)
			}
		}
		for _, a := range instance.Header.Sender {
			ma := a.ToMailAddress()
			if lygo.Arrays.IndexOf(ma.String(), tmp)==-1{
				tmp = append(tmp, ma.String())
				response = append(response, ma)
			}
		}
	}
	return response
}

func (instance *MailboxerMessage) ReplyTo() []*mail.Address {
	response := make([]*mail.Address, 0)
	if nil != instance && nil != instance.Header {
		for _, a := range instance.Header.ReplyTo {
			response = append(response, a.ToMailAddress())
		}
	}
	return response
}

func (instance *MailboxerMessage) Attachments() []*MailboxerAttachment {
	response := make([]*MailboxerAttachment, 0)
	if nil != instance && nil != instance.Body {
		return instance.Body.Attachments
	}
	return response
}

func (instance *MailboxerMessage) Date() time.Time {
	if nil != instance && nil != instance.Header {
		return instance.Header.Date
	}
	return time.Now()
}

func (instance *MailboxerMessage) SaveToFile(args ...interface{}) (err error) {
	filename := lygo_strings.Slugify(instance.Subject()) + ".eml"
	if len(args) > 0 {
		filename = lygo_conv.ToString(args[0])
	}
	ext := strings.ToLower(lygo_paths.ExtensionName(filename))
	if ext == "json" {
		_, err = lygo_io.WriteTextToFile(instance.Json(), filename)
	} else if len(instance.BodyData) > 0 {
		if len(ext) == 0 {
			filename = filename + ".eml"
		}
		_, err = lygo_io.WriteBytesToFile(instance.BodyData, filename)
	}
	return
}

func (instance *MailboxerMessage) LoadFromFile(filename string) (err error) {
	data, err := lygo_io.ReadBytesFromFile(filename)
	if nil != err {
		return err
	}
	envelope, err := parseBodyData(data, instance)
	if nil != err {
		return err
	}
	if nil != envelope {
		parseMime(envelope, instance)
	}
	return nil
}

func (instance *MailboxerMessage) ParseImap(m *imap.Message) *MailboxerMessage {
	if nil != m {
		parseImap(m, instance)
	}
	return instance
}

func (instance *MailboxerMessage) Parse(arg interface{}) (err error) {
	var data []byte
	switch v := arg.(type) {
	case io.Reader:
		data, err = ioutil.ReadAll(v)
	case []byte:
		data = v
	case string:
		data = []byte(v)
	case *imap.Message:
		parseImap(v, instance)
	}
	if nil != err {
		return err
	}

	if len(data) > 0 {
		envelope, err := parseBodyData(data, instance)
		if nil != err {
			return err
		}
		if nil != envelope {
			parseMime(envelope, instance)
		}
	}

	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e    (MailboxerMessage )
// ---------------------------------------------------------------------------------------------------------------------

func parseMime(envelope *mailboxer_mime.Envelope, target *MailboxerMessage) {

}

func parseImap(source *imap.Message, target *MailboxerMessage) {
	if nil != source && nil != target {
		target.SeqNum = source.SeqNum
		target.InternalDate = source.InternalDate
		if nil == target.Header {
			target.Header = NewMailboxerMessageHeader()
		}
		if nil == target.Body {
			target.Body = NewMailboxerMessageBody()
		}

		// ENVELOPE
		if len(target.Header.MessageId) == 0 {
			envelope := source.Envelope
			if nil != envelope {
				target.Header.MessageId = envelope.MessageId
				target.Header.Subject = envelope.Subject
				target.Header.Date = envelope.Date
				target.Header.ParentMessageId = envelope.InReplyTo // parent MessageId

				// REPLY-TO
				target.Header.ReplyTo = make([]*MailboxerAddress, 0)
				for _, a := range envelope.ReplyTo {
					address := &MailboxerAddress{
						PersonalName: a.PersonalName,
						AtDomainList: a.AtDomainList,
						MailboxName:  a.MailboxName,
						HostName:     a.HostName,
					}
					target.Header.ReplyTo = append(target.Header.ReplyTo, address)
				}
				// SENDER
				target.Header.Sender = make([]*MailboxerAddress, 0)
				for _, a := range envelope.Sender {
					address := &MailboxerAddress{
						PersonalName: a.PersonalName,
						AtDomainList: a.AtDomainList,
						MailboxName:  a.MailboxName,
						HostName:     a.HostName,
					}
					target.Header.Sender = append(target.Header.Sender, address)
				}
				// FROM
				target.Header.From = make([]*MailboxerAddress, 0)
				for _, a := range envelope.From {
					address := &MailboxerAddress{
						PersonalName: a.PersonalName,
						AtDomainList: a.AtDomainList,
						MailboxName:  a.MailboxName,
						HostName:     a.HostName,
					}
					target.Header.From = append(target.Header.From, address)
				}
				// TO
				target.Header.To = make([]*MailboxerAddress, 0)
				for _, a := range envelope.To {
					address := &MailboxerAddress{
						PersonalName: a.PersonalName,
						AtDomainList: a.AtDomainList,
						MailboxName:  a.MailboxName,
						HostName:     a.HostName,
					}
					target.Header.To = append(target.Header.To, address)
				}
				// CC
				target.Header.Cc = make([]*MailboxerAddress, 0)
				for _, a := range envelope.Cc {
					address := &MailboxerAddress{
						PersonalName: a.PersonalName,
						AtDomainList: a.AtDomainList,
						MailboxName:  a.MailboxName,
						HostName:     a.HostName,
					}
					target.Header.Cc = append(target.Header.Cc, address)
				}
				// BCC
				target.Header.Bcc = make([]*MailboxerAddress, 0)
				for _, a := range envelope.Bcc {
					address := &MailboxerAddress{
						PersonalName: a.PersonalName,
						AtDomainList: a.AtDomainList,
						MailboxName:  a.MailboxName,
						HostName:     a.HostName,
					}
					target.Header.Bcc = append(target.Header.Bcc, address)
				}
			}
		}

		// BODY STRUCTURE
		if nil != source.BodyStructure {
			// The children parts, if multipart.
			parseImapBodyStructure(source.BodyStructure, source.Body, target)
		}
	}
}

func parseImapBodyStructure(source *imap.BodyStructure, body map[*imap.BodySectionName]imap.Literal, targetMessage *MailboxerMessage) {
	if nil == targetMessage.Body {
		targetMessage.Body = new(MailboxerMessageBody)
	}
	targetBody := targetMessage.Body
	// copy body structure into target output message
	imapMergeBodyStructure(source, targetBody)

	if len(body) > 0 {
		for _, r := range body {
			_ = parseBody(r, targetMessage)
		}
	}

}

func imapMergeBodyStructure(source *imap.BodyStructure, target *MailboxerMessageBody) {
	target.Id = source.Id
	target.Description = source.Description
	target.MIMEType = source.MIMEType
	target.MIMESubType = source.MIMESubType
	target.Params = source.Params
	target.Encoding = source.Encoding
	target.Size = source.Size
	target.Extended = source.Extended
	target.MD5 = source.MD5
	target.Disposition = source.Disposition
	target.DispositionParams = source.DispositionParams
	target.Language = source.Language
	target.Location = source.Location
	target.PartsCount = len(source.Parts)
	target.Multipart = len(source.Parts) > 0
}

func parseBody(r io.Reader, targetMessage *MailboxerMessage) error {
	data, err := ioutil.ReadAll(r)
	if nil != err {
		return err
	}
	_, err = parseBodyData(data, targetMessage)
	return err
}

func parseBodyData(data []byte, targetMessage *MailboxerMessage) (*mailboxer_mime.Envelope, error) {
	targetMessage.BodyData = data
	if nil == targetMessage.Body {
		targetMessage.Body = new(MailboxerMessageBody)
	}

	// mime parser
	env, envErr := mailboxer_mime.ReadEnvelope(bytes.NewReader(data))
	if nil == envErr && nil != env {
		// content
		targetMessage.Body.Text = env.Text
		targetMessage.Body.HTML = env.HTML

		// headers
		targetMessage.Body.Headers = make(map[string]string)
		keys := env.GetHeaderKeys()
		for _, key := range keys {
			value := env.GetHeader(key)
			targetMessage.Body.Headers[key] = value
		}

		// attachments
		if len(env.Attachments) > 0 {
			targetMessage.Body.Attachments = make([]*MailboxerAttachment, 0)
			for _, attachment := range env.Attachments {
				targetMessage.Body.Attachments = append(targetMessage.Body.Attachments, &MailboxerAttachment{
					Filename: attachment.FileName,
					Content:  attachment.Content,
				})
			}
		}

		// is header ready?
		if nil == targetMessage.Header {
			parseHeader(env, targetMessage)
		}

	}
	return env, nil
}

func parseHeader(env *mailboxer_mime.Envelope, target *MailboxerMessage) {
	if nil == target.Header {
		target.Header = NewMailboxerMessageHeader()
	}
	if nil == target.Body {
		target.Body = NewMailboxerMessageBody()
	}

	// HEADER
	if len(target.Header.MessageId) == 0 {
		target.Header.MessageId = env.GetHeader(HeaderMessageId)
	}
	if len(target.Header.ParentMessageId) == 0 {
		target.Header.ParentMessageId = env.GetHeader(HeaderParentMessageId)
	}
	if lygo_date.IsZero(target.Header.Date) {
		dt, err := lygo_date.ParseAny(env.GetHeader(HeaderDate))
		if nil==err{
			target.Header.Date = dt
		}
	}
	if len(target.Header.Subject) == 0 {
		target.Header.Subject = env.GetHeader(HeaderSubject)
	}
	if len(target.Header.Sender) == 0 {
		target.Header.SetSender(env.GetHeader(HeaderSender))
	}
	if len(target.Header.ReplyTo) == 0 {
		target.Header.SetReplyTo(env.GetHeader(HeaderReplyTo))
	}
	if len(target.Header.From) == 0 {
		target.Header.SetFrom(env.GetHeader(HeaderFrom))
	}
	if len(target.Header.To) == 0 {
		target.Header.SetTo(env.GetHeader(HeaderTo))
	}
	if len(target.Header.Cc) == 0 {
		target.Header.SetCc(env.GetHeader(HeaderCc))
	}
	if len(target.Header.Bcc) == 0 {
		target.Header.SetBcc(env.GetHeader(HeaderBcc))
	}

	// MESSAGE & BODY
	if len(target.Body.MIMEType) == 0 {
		target.Body.SetMIMEType(env.GetHeader(HeaderContentType))
	}
	if lygo_date.IsZero(target.InternalDate) {
		dt, err := lygo_date.ParseAny(env.GetHeader(HeaderDate))
		if nil==err{
			target.InternalDate = dt
		}
	}

}
